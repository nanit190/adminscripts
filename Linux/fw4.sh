#!/bin/sh
# fw4.sh: iptables IPv4 firewall configuration script

# Environment
IPT="/sbin/iptables"
ECHO="/bin/echo"

# Clean up first
$IPT -F
$IPT -X
$IPT -t nat -F
$IPT -t nat -X
$IPT -t mangle -F
$IPT -t mangle -X

# Interface(s)
PUB_IF="eth0"

# Allow everything on looback
$ECHO "iptables: Loopback"
$IPT -A INPUT -i lo -j ACCEPT
$IPT -A OUTPUT -o lo -j ACCEPT

# Default policy to DROP
$ECHO "iptables: Default policy to DROP"
$IPT -P INPUT DROP
$IPT -P FORWARD DROP
$IPT -P OUTPUT DROP

# Allow all outcoming connection, and established connections
$ECHO "iptables: Allow outcoming and established connection"
$IPT -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPT -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

#### Services

$ECHO "iptables: Services:"

# SSH
$ECHO "...SSH"
$IPT -A INPUT -i ${PUB_IF} -p tcp --destination-port 22 -j ACCEPT			# Highly recommended to limit source ips\nets with -s option after -p tcp

# NTP packets
$ECHO "...NTP"
$IPT -A INPUT -p udp --source-port 123 -j ACCEPT

# ICMP (some types)
$ECHO "...ICMP"
$IPT -A INPUT -p icmp --icmp-type 0 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
$IPT -A INPUT -p icmp --icmp-type 3 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
$IPT -A INPUT -p icmp --icmp-type 8 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
$IPT -A INPUT -p icmp --icmp-type 11 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

# IGMP (only if you need IGMP)
#$ECHO "...IGMP"
#$IPT -A INPUT -i ${HOME_IF} -p igmp -j ACCEPT

#### Special services

# HTTP/HTTPS service (for example)
#$IPT -A INPUT -i ${PUB_IF} -p tcp --destination-port 80 -j ACCEPT
#$IPT -A INPUT -i ${PUB_IF} -p tcp --destination-port 443 -j ACCEPT

####

# Drop NETBIOS junk
$ECHO "iptables: Drop NETBIOS"
$IPT -A INPUT -p tcp --dport 137:139 -j REJECT
$IPT -A INPUT -p udp --dport 137:139 -j REJECT

# Log everything else (when necessary)
#$IPT -A INPUT -j LOG
#$IPT -A FORWARD -j LOG

# Done
exit 0

